class FishModel {
  final String fisheryManagement;
  // final List<Image> gallery;
  final String management;
  final Illutration illustration;

  const FishModel({
    required this.fisheryManagement,
    // required this.gallery,
    required this.illustration,
    required this.management,
  });

  factory FishModel.fromJson(Map<String, dynamic> json) => FishModel(
      fisheryManagement: json['Fishery Management'] ?? 'No management',
      // gallery: List.from(json['Image Gallery'] ?? [])
      //     .map((e) => Image.fromJson(e))
      //     .toList(),
      illustration: Illutration.fromJson(
        json['Species Illustration Photo'] ?? 'No Illustration',
      ),
      management: json['Management'] ?? 'No management');
}

class Image {
  final String image;
  const Image({required this.image});

  factory Image.fromJson(Map<String, dynamic> json) =>
      Image(image: json['src']);
}

class Illutration {
  final String image;
  const Illutration({required this.image});

  factory Illutration.fromJson(Map<String, dynamic> json) =>
      Illutration(image: json['src']);
}
