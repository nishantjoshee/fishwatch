import 'package:fishwatch/api_service.dart';
import 'package:fishwatch/fish_model.dart';
import 'package:flutter/material.dart';

class FishUi extends StatefulWidget {
  const FishUi({Key? key}) : super(key: key);

  @override
  _FishUiState createState() => _FishUiState();
}

class _FishUiState extends State<FishUi> {
  @override
  void initState() {
    ApiService().getFish();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Fish"),
      ),
      body: FutureBuilder<List<FishModel>>(
        future: ApiService().getFish(),
        builder: (context, snap) {
          if (snap.hasData) {
            return ListView(children: [
              ...snap.data!.map(
                (e) => Text(e.management),
              ),
            ]);
          } else if (snap.hasError) {
            return Center(
              child: Text(snap.error.toString()),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
