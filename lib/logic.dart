import 'package:fishwatch/api_service.dart';
import 'package:fishwatch/fish_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final fishDataProvider = FutureProvider<List<FishModel>>((ref) async {
  return ref.read(apiProvider).getFish();
});

final fishLogicProvider = StateNotifierProvider<FishNotifier, FishState>((ref) {
  return FishNotifier(ref);
});

class FishNotifier extends StateNotifier<FishState> {
  FishNotifier(this.ref) : super(FishInitial());
  final StateNotifierProviderRef ref;

  getData() async {
    state = FishLoading();
    try {
      final _data = await ref.read(apiProvider).getFish();
      state = FishLoaded(fishes: _data);
    } catch (e) {
      state = FishError(error: e.toString());
    }
  }
}

abstract class FishEvent {}

class FishLoadEvent extends FishEvent {}

abstract class FishState {}

class FishInitial extends FishState {}

class FishLoading extends FishState {}

class FishError extends FishState {
  final String error;
  FishError({required this.error});
}

class FishLoaded extends FishState {
  final List<FishModel> fishes;
  FishLoaded({required this.fishes});
}
