import 'dart:convert';
import 'package:fishwatch/api_client.dart';
import 'package:fishwatch/fish_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart';

class ApiService {
  static const String endpoint = 'https://www.fishwatch.gov/api/species';

  Future<List<FishModel>> getMyFish() async {
    final List data = ApiClient().getData(species: 'species');
    return data.map((e) => FishModel.fromJson(e)).toList();
  }

  Future<List<FishModel>> getFish() async {
    final Response response = await get(Uri.parse(endpoint));
    if (response.statusCode == 200) {
      final List result = json.decode(response.body);
      return result.map((e) => FishModel.fromJson(e)).toList();
    } else {
      throw Exception(response.reasonPhrase);
    }
  }
}

final apiProvider = Provider<ApiService>((ref) {
  return ApiService();
});
